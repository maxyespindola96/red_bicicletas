var map = L.map('mapid').setView([-27.4860818,-58.8624273], 12);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {
    foo: 'bar', 
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
}).addTo(map);


$.ajax({
    dataType: "json",
    url: "/api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
});
