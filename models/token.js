var mongoose = require('mongoose');
var schema = mongoose.Schema;

var tokenSchema = new schema({
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario'
    },
    token:{
        type: String
    },
    createdIn: Date
});