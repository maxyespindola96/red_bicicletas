"use strict";

var _this = void 0;

var Bicicleta = function Bicicleta(id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
}; //NOTA: no se puede usar los arrows functions como constructor


Bicicleta.allBicis = [];

Bicicleta.prototype.toString = function () {
  return "id: ".concat(_this.id, " | color: ").concat(_this.color, " | modelo: ").concat(_this.modelo, " | ubicacion: ").concat(_this.ubicacion);
};

Bicicleta.add = function (bici) {
  return Bicicleta.allBicis.push(bici);
};

Bicicleta.findById = function (biciID) {
  var bici = Bicicleta.allBicis.find(function (x) {
    return x.id == biciID;
  });
  if (bici) return bici;else throw new Error("No existe una bicicleta con la id ".concat(biciID));
};

Bicicleta.APIFindById = function (biciID, res) {
  var bici = Bicicleta.allBicis.find(function (x) {
    return x.id == biciID;
  });
  if (bici) return bici;else throw res.status(404).json({
    message: "No existe una bicicleta con la id ".concat(biciID)
  });
};

Bicicleta.removeById = function (biciID) {
  //Bicicleta.findById(biciID);
  for (var i = 0; i < Bicicleta.allBicis.length; i++) {
    if (Bicicleta.allBicis[i].id == biciID) {
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
};

var bici1 = new Bicicleta(1, 'rojo', 'urbana', [-27.467654, -58.844231]);
var bici2 = new Bicicleta(2, 'blanca', 'urbana', [-27.466055, -58.843545]);
Bicicleta.add(bici1);
Bicicleta.add(bici2);
module.exports = Bicicleta;