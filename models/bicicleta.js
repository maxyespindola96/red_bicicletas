var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        createIndexes: {
            type: '2dsphere',
            sparse: true
        }
    }
});

bicicletaSchema.method.toString = () => `Code: ${this.code} - Color: ${this.color}`;

bicicletaSchema.statics.allBicis = function(cb){ 
    return this.find({}, cb);
}

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

bicicletaSchema.statics.add = function(bici, cb) {
    return this.create(bici, cb)
};

bicicletaSchema.statics.findByCode = function(biciCode, cb){
    return this.findOne({code: biciCode}, cb);
}

bicicletaSchema.statics.removeByCode = function(biciCode, cb){
    return this.deleteOne({code: biciCode}, cb);
}

bicicletaSchema.statics.updateByCode = function(biciCode, updateBici, cb){
    return this.findOneAndUpdate({code: parseInt(biciCode)}, {$set: updateBici}, {new: true}, cb);
}

module.exports = mongoose.model('Bicicletas', bicicletaSchema);
