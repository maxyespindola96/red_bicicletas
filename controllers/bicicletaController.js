var Bicicleta = require('../models/bicicleta');

// INDEX
exports.bicicleta_list = (req, res) =>{
    res.render('bicicletas/index', { bicis: Bicicleta.allBicis });
}

// CREATE
// Mostrar formulario para crear una bicicleta
exports.bicicleta_create_get = (req, res) =>{
    res.render('bicicletas/create');
}
// Agregar una nueva bici
 exports.bicicleta_create_post = (req, res) =>{
    let data = req.body;
    let bici = new Bicicleta(data.id, data.color, data.modelo);
    bici.ubicacion = [data.lat, data.lng];

    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

// DELETE
exports.bicicleta_delete_post = (req, res) => {
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}
// UPDATE
// Mostrar formulario para actualizar una bici
exports.bicicleta_update_get = (req, res) =>{
    let bici = Bicicleta.findById(req.params.id); // ID que proviene desde el parametro

    res.render('bicicletas/update', { bici });
}
// Actualizar una bici
 exports.bicicleta_update_post = (req, res) =>{
    let data = req.body;
    let bici = Bicicleta.findById(req.params.id);
    
    bici.id = data.id;
    bici.color = data.color;
    bici.modelo = data.modelo;
    bici.ubicacion = [data.lat, data.lng];

    res.redirect('/bicicletas');
}
// SHOW