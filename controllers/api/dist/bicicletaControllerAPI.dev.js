"use strict";

var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res) {
  res.status(200).json({
    bicicletas: Bicicleta.allBicis
  });
};

exports.bicicleta_create = function (req, res) {
  var data = req.body;
  var bici = new Bicicleta(data.id, data.color, data.modelo);
  bici.ubicacion = [data.lat, data.lng];
  Bicicleta.add(bici);
  res.status(200).json({
    bicicleta: bici
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeById(req.body.id); // 204 - No hay contenido en la respuesta

  res.status(204).send();
};

exports.bicicleta_update = function (req, res) {
  var bici = Bicicleta.APIFindById(req.params.id, res);
  var data = req.body;
  bici.id = data.id;
  bici.color = data.color;
  bici.modelo = data.modelo;
  bici.ubicacion = [data.lat, data.lng];
  res.status(200).json({
    bicicleta: bici
  });
};