var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

const mostrarTexto = () =>{
    beforeEach(()=>{
        console.log("Testeano...");
    });
}

describe('Bicicletas API', () => {
    mostrarTexto();

    describe('GET BICICLETAS /', () => {
        it('Status code 200', (done) => {
            request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done(); //Si o si debe ejecutarse esta funcion
            });
        });
    });

    describe('POST BICICLETAS / CREATE', () => {
        it('Status code 200', (done) => {
            var headers = {'content-type' : 'application/json'};

            var bici1 = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -27.467654, "lng": -58.844231 }';

            request.post({
                    headers:    headers,
                    url:        'http://localhost:3000/api/bicicletas/create',
                    body:       bici1
                }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(10, (err, targetBici) => {
                    expect(targetBici.code).toBe(10);
                    expect(targetBici.color).toBe("rojo");
                    expect(targetBici.modelo).toBe("urbana");
                    expect(targetBici.ubicacion[0]).toBe(-27.467654);
                    expect(targetBici.ubicacion[1]).toBe(-58.844231);

                    done();
                });
                //expect(Bicicleta.APIFindById(10, response).color).toBe("rojo"); - Test exitoso
                //expect(Bicicleta.APIFindById(9, response)).toThrow();
            });
        });
    });

    //DELETE
    describe('DELETE BICICLETAS / DELETE', () => {
        it('Status code 204', (done) => {
            var headers = {'content-type' : 'application/json'};

            // var bici1 = new Bicicleta(10, "rojo", "urbana");
            // Bicicleta.add(bici1);
            // Eliminar la bici que se creo en el test anterior
            request.delete({
                    headers:    headers,
                    url:        'http://localhost:3000/api/bicicletas/delete',
                    body:       '{"code": 10}'
                }, (error, response, body) => {
                expect(response.statusCode).toBe(204);
                done(); //Si o si debe ejecutarse esta funcion
            });
        });
    });
    //UPDATE
    describe('UPDATE BICICLETAS / UPDATE', () => {
        it('Status code 200', (done) => {
            var headers = {'content-type' : 'application/json'};

            var bici1 = new Bicicleta({code: 9, color: "rojo", modelo: "urbana"});
            bici1.save();

            var biciModificada = '{"code": 9, "color": "azul", "modelo": "clasica"}';

            request.put({
                    headers:    headers,
                    url:        'http://localhost:3000/api/bicicletas/update/9',
                    body:       biciModificada
                }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(9, (err, targetBici) => {
                    expect(targetBici.code).toBe(9);
                    expect(targetBici.color).toBe("azul");
                    expect(targetBici.modelo).toBe("clasica");
                    done();
                });
            });
        });
    });
});