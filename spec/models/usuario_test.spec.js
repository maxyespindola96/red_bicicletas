var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
var Bicicleta = require('../../models/bicicleta');

describe('Testing de usuarios', function(){
    beforeEach(function(done) { 
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error '));
        db.once('open', function(){
            console.log('We are connected to test Database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, (err, success) => {
            if(err) console.log(err);
            Usuario.deleteMany({}, (err, success) => {
                if(err) console.log(err);
                Bicicleta.deleteMany({}, (err, success) => {
                    if(err) console.log(err)
                    mongoose.connection.close();// Despues de finalizar un test hay que cerrar la conexion 
                    done();                     // si no habra un error de timeout
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () =>{
        it('Debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: "Maxy"});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            bicicleta.save();

            var hoy = new Date();
            var maniana = new Date(); 
            
            maniana.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, maniana, (err, reserva) => {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec((err, reservas) => {
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});