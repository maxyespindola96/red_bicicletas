var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing de bicicleta', function(){
    beforeEach(function(done) { 
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error '));
        db.once('open', function(){
            console.log('We are connected to test Database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) {
                console.log(err);
            }
            mongoose.connection.close();// Despues de finalizar un test hay que cerrar la conexion 
            done();                     // si no habra un error de timeout
        });
    });

    
    describe('Bicicleta.allBicis', () => {
        it('Comienza vacio', (done) => {

            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
            });
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', (done) =>{   
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
            done();
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega solo una bici', (done) => {
            var bici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(bici, (err, newBici) => {
                if(err) console.log(err);
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(bici.code);
                    done();
                });
            }); 
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver una bicicleta con el code 1', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0);

                var bici1 = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(bici1, (err, newBici) => {
                    if(err) console.log(err);

                    var bici2 = new Bicicleta({code: 2, color: "rojo", modelo: "urbana"});
                    Bicicleta.add(bici2, (err, newBici) => {
                        if(err) console.log(err);

                        Bicicleta.findByCode(1, (err, targetBici) => {
                            expect(targetBici.code).toBe(bici1.code);
                            expect(targetBici.color).toBe(bici1.color);
                            expect(targetBici.modelo).toBe(bici1.modelo);
                            done();
                        });
                    }); 
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Eliminar bicicleta con code 1', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                if(err) console.log(err);
                expect(bicis.length).toBe(0);

                var bici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(bici, (err, newBici) => {
                    if(err) console.log(err)

                    Bicicleta.allBicis((err, bicis) => {
                        if(err) console.log(err);
                        expect(bicis.length).toBe(1);
                        
                        Bicicleta.removeByCode(1, (err, targetBici) => {            
                            if(err) console.log(err);
                            Bicicleta.allBicis((err, bicis) => {
                                if(err) console.log(err);
                                expect(bicis.length).toBe(0);
                                done();
                            });         
                        });
                    });     
                });
            });
        });
    });
});






//Ejecutar este codigo antes de cada test
// beforeEach(() => {
//     Bicicleta.allBicis = [];
// }); 

// describe('Bicicleta.allBicis', () => {
//     it('Comienza vacio', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () =>{
//     it('Agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
        
//         var bici1 = new Bicicleta(1, 'rojo', 'urbana', [-27.467654, -58.844231]);
//         Bicicleta.add(bici1);

//         expect(Bicicleta.allBicis.length).toBe(1);

//         expect(Bicicleta.allBicis[0]).toBe(bici1);
//     });
// });

// describe('Bicicleta.findById', () => {
//     it('Retornar bici con id 1', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var bici1 = new Bicicleta(1, 'verde', 'urbana');
//         var bici2 = new Bicicleta(2, 'azul', 'montaña');
//         Bicicleta.add(bici1);
//         Bicicleta.add(bici2);

//         var targetBici = Bicicleta.findById(1);

//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(bici1.color);
//         expect(targetBici.modelo).toBe(bici1.modelo);

//     });
// });


// describe('Bicicleta.removeById', () => {
//     it('Borrar bici con id 1', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var bici1 = new Bicicleta(1, 'verde', 'urbana');
//         var bici2 = new Bicicleta(2, 'azul', 'montaña');
//         Bicicleta.add(bici1);
//         Bicicleta.add(bici2);

//         var id = 1;

//         Bicicleta.removeById(id);

//         expect(() => {
//             Bicicleta.findById(id)
//         }).toThrowError(`No existe una bicicleta con la id ${id}`);
//     });
// });