var express = require('express');
var router = express.Router();
var usuarioControllerAPI = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioControllerAPI.usuarios_list);

router.post('/create', usuarioControllerAPI.usuarios_create);

router.post('/reservar', usuarioControllerAPI.usuarios_reservar)

//router.delete('/delete', usuarioControllerAPI.bicicleta_delete);

//router.put('/update/:id', usuarioControllerAPI.bicicleta_update);

module.exports = router;