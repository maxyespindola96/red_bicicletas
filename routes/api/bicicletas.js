var express = require('express');
var router = express.Router();
var bicicletasControllerAPI = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletasControllerAPI.bicicleta_list);

router.post('/create', bicicletasControllerAPI.bicicleta_create);

router.delete('/delete', bicicletasControllerAPI.bicicleta_delete);

router.put('/update/:id', bicicletasControllerAPI.bicicleta_update);

module.exports = router;